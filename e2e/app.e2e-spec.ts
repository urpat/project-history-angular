import { ProjectHistoryAngularPage } from './app.po';

describe('project-history-angular App', function() {
  let page: ProjectHistoryAngularPage;

  beforeEach(() => {
    page = new ProjectHistoryAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
