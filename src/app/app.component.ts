import { Component, OnInit } from '@angular/core';

import { AuthService } from "./auth/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	userName;
	userRole;
  userId;

	constructor(private authService: AuthService) {}

  getUserParams(): void {
    this.userName = localStorage.getItem('name');
    this.userRole = localStorage.getItem('role');
    this.userId = localStorage.getItem('id');
  }

  ngOnInit() {
    this.getUserParams();
  }

    client() {
    	if(this.userRole == '3') {
      		return true;
    	}
  	}

  	development() {
    	if(this.userRole == '2') {
      		return true;
    	}
  	}

}
