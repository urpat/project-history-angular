import { Component, OnInit, Input } from '@angular/core';
import { Response } from "@angular/http";
import { NgForm } from '@angular/forms';

import { Project } from "../project.interface";
import { ProjectService } from "../project.service";
import { AppComponent } from "../../app.component";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  project: Project;
  projects: Project[];
  list = true;
  create = false;

  constructor(private projectService: ProjectService, private app: AppComponent) {}

  getProjects(): void {
    this.projectService.getProjects()
      .map(res => res.json())
      .subscribe(
        data => this.projects = data.projects,
        error => console.log(error)
    );
  }

  ngOnInit(): void {
    this.app.getUserParams();
    this.getProjects();
  }

  onCreate() {
    this.create = true;
    this.list = false;
  }

  onCancel() {
    this.create = false;
    this.list = true;
  }

  onDelete(project: Project) {
    const position = this.projects.findIndex(
      (projectEl: Project) => {
        return projectEl.id == project.id;
      }
    );
    this.projects.splice(position, 1);
  }

  onSubmit(form: NgForm) {
    this.projectService.createProject(form.value.name)
      .subscribe(
        (data: Response) => {
          this.project = data.json().project;
          console.log('Project created');
          this.projects.unshift(this.project);
        }
      );
    form.reset();
    this.create = false;
    this.list = true;
  }

}