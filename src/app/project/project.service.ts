import { Injectable } from "@angular/core";
import { Http, Response, Headers } from "@angular/http";
import 'rxjs/Rx';
import { Observable } from "rxjs";
//import { AuthHttp } from '../auth/angular2-jwt';

import { Project } from "./project.interface";

@Injectable()
export class ProjectService {
	project: Project;
	token = localStorage.getItem('id_token');

	constructor(private http: Http) {
	}

	getProjects(): Observable<any> {
		const headers = new Headers({'Authorization': 'Bearer' + this.token});
		return this.http.get('http://project-history.lc/api/project', {headers: headers});
	}

	createProject(name: string): Observable<any> {
		const body = JSON.stringify({name: name});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/project/create', body, {headers: headers});
	}

	updateProject(id: number, newName: string) {
		const body = JSON.stringify({name: newName});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.put('http://project-history.lc/api/project/edit/' + id, body, {headers: headers})
			.map(
				(response: Response) => response.json(),
			);
	}

	deleteProject(id: number) {
		const headers = new Headers({'Authorization': 'Bearer' + this.token});
		return this.http.delete('http://project-history.lc/api/project/delete/' + id, {headers: headers});
	}

	addUserProject(user: string, projectId: number): Observable<any> {
		const body = JSON.stringify({user: user, projectId: projectId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/user_project', body, {headers: headers});
	}

	getUserProject(projectId: number) {
		const body = JSON.stringify({projectId: projectId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/get_user_project', body, {headers: headers});
	}

	deleteUserProject(userId: number, projectId: number) {
		const body = JSON.stringify({userId: userId, projectId: projectId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/user_project/delete', body, {headers: headers});
	}
}