import { Injectable } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';
import { Observable } from "rxjs";
import { default as swal } from 'sweetalert2';

@Injectable()
export class AuthService {

  constructor(
    private http: Http,
    private router: Router
  ) {}

  login(email: string, password: string): void {
    const body = JSON.stringify({email: email, password: password});
    const headers = new Headers({'Content-Type': 'application/json'});
    this.http.post('http://project-history.lc/api/login', body, {headers: headers})
      .map(res => res.json())
      .subscribe(
        data => {
          localStorage.setItem('id_token', data.token);
          localStorage.setItem('name', data.user.name);
          localStorage.setItem('role', data.user.role_id);
          localStorage.setItem('id', data.user.id);

          this.router.navigate(['']);
        },
        error => {
          console.log(error);
          swal({
            title: "Ошибка входа!",
            type: "error"
          });
        }
      );
  }

  register(name:string, email: string, password: string, role: number): void {
    const body = JSON.stringify({name: name, email: email, password: password, role: role});
    const headers = new Headers({'Content-Type': 'application/json'});
    this.http.post('http://project-history.lc/api/register', body, {headers: headers})
      .map(res => res.json())
      .subscribe(
        data => {
          localStorage.setItem('id_token', data.token);
          localStorage.setItem('name', data.user.name);
          localStorage.setItem('role', data.user.role_id);
          localStorage.setItem('id', data.user.id);
          
          this.router.navigate(['']);
        },
        error => {
          console.log(error);
          swal({
            title: "Ошибка регистрации!",
            type: "error"
          });
        }
      );
  }

  loggedIn() {
    return tokenNotExpired();
  }

  logout() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('name');
    localStorage.removeItem('role');
    localStorage.removeItem('id');
    this.router.navigate(['login']); 
  }

}