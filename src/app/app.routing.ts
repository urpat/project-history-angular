import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule, CanActivate } from "@angular/router";
import { AuthGuard } from './auth/auth-guard.service';

import { RegisterComponent } from "./auth/register/register.component";
import { LoginComponent } from "./auth/login/login.component";
import { ProjectsComponent } from "./project/projects/projects.component";
import { MeetingsComponent } from "./meeting/meetings/meetings.component";
import { ShowMeetingComponent } from "./meeting/show-meeting/show-meeting.component";

const APP_ROUTES: Routes = [
	{ path: '', component: ProjectsComponent, canActivate: [AuthGuard] },
	{ path: 'project/:id', component: MeetingsComponent, canActivate: [AuthGuard] },
	{ path: 'project/:project_id/meeting/:id', component: ShowMeetingComponent, canActivate: [AuthGuard] },
	{ path: 'register', component: RegisterComponent },
	{ path: 'login', component: LoginComponent },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);