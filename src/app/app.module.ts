import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Http, HttpModule } from '@angular/http';
import { routing } from "./app.routing";
import { Ng2CompleterModule } from "ng2-completer";

import { AppComponent } from './app.component';
import { ProjectsComponent } from './project/projects/projects.component';

import { ProjectService } from "./project/project.service";
import { ProjectComponent } from './project/project/project.component';
import { MeetingsComponent } from './meeting/meetings/meetings.component';
import { MeetingComponent } from './meeting/meeting/meeting.component';
import { MeetingService } from "./meeting/meeting.service";
import { MeetingTaskComponent } from './meeting/meeting-task/meeting-task.component';
import { ShowMeetingComponent } from './meeting/show-meeting/show-meeting.component';
import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';

import { AuthService } from "./auth/auth.service";
import { AuthGuard } from "./auth/auth-guard.service";

@NgModule({
  declarations: [
    AppComponent,
    ProjectsComponent,
    ProjectComponent,
    MeetingsComponent,
    MeetingComponent,
    MeetingTaskComponent,
    ShowMeetingComponent,
    RegisterComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    Ng2CompleterModule
  ],
  providers: [
    ProjectService,
    MeetingService,
    AuthGuard,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
