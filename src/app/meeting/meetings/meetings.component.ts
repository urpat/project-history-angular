import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { CompleterService, CompleterData } from 'ng2-completer';
import { default as swal } from 'sweetalert2';

import { Response } from "@angular/http";
import { NgForm } from '@angular/forms';

import { Meeting } from "../meeting.interface";
import { MeetingService } from "../meeting.service";
import { ProjectService } from "../../project/project.service";
import { AppComponent } from "../../app.component";

@Component({
  selector: 'app-meetings',
  templateUrl: './meetings.component.html',
  styleUrls: ['./meetings.component.css']
})
export class MeetingsComponent implements OnInit, OnDestroy {
  create = false;
  meeting: Meeting;
  meetings: Meeting[];
  projectName = '';
  userList = false;
  @Input() userProject;
  selectedValue: string;
  id: number;
  private subscription: Subscription;
  date;

  private searchStr: string;
  private dataService: CompleterData;

  constructor(
  	private activateRoute: ActivatedRoute,
  	private meetingService: MeetingService,
    private projectService: ProjectService,
    private app: AppComponent,
    private completerService: CompleterService
  ) { 
    this.subscription = activateRoute.params.subscribe(params=>this.id=params['id']);
    this.dataService = completerService.remote("http://project-history.lc/api/user?", 'name', 'name');
  }

  getMeetings(): void {
  	this.meetingService.getMeetings(this.id)
        .subscribe(
            (data: Response) => {
            	this.meetings = data.json().meetings;
              this.projectName = data.json().project.name;
            }
  		);
  }

  getUserProject(): void {
    this.projectService.getUserProject(this.id)
        .subscribe(
            (data: Response) => {
              this.userProject = data.json().userProject;
            }
      );
  }

  ngOnInit(): void {
    this.getUserProject();
  	this.getMeetings();
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  onCreate() {
  	this.create = true;
  }

  onCancel() {
    this.searchStr = '';
  	this.create = false;
    this.date = '';
    this.userList = false;
  }

  createMeeting() {
    this.meetingService.createMeeting(this.date, this.id)
      .subscribe(
        (data: Response) => {
          this.meeting = data.json().meeting;
          console.log('Meeting created');
          this.meetings.unshift(this.meeting);

            this.meetingService.createMeetingUser(this.meeting.id, this.app.userId)
              .subscribe(
                (data: Response) => {
                  console.log('Meeting user create');
                }
              );
        }
      );
    this.date = '';
    this.create = false;
  }

  deleteElement(meetingId: number) {
    const position = this.meetings.findIndex(
      (meetingEl: Meeting) => {
        return meetingEl.id == meetingId;
      }
    );
    this.meetings.splice(position, 1);
  }

  addUser() {
    this.userList = true;
  }

  addValue() {
    if (this.searchStr) {
      const diff = this.userProject.find(
        (user) => {
          return user.name == this.searchStr;
        });

      if (!diff) {
        this.projectService.addUserProject(this.searchStr, this.id)
          .subscribe(
            data => {
              this.userProject.unshift(data.json().user);
            }
          );
        this.searchStr = '';
        this.userList = false;
      } else {
        swal({
          title: "Пользователь уже участвует в проекте!",
          type: "error"
        })
        this.searchStr = '';
      }
    }
  }

  deleteUser(userId: number) {
    this.projectService.deleteUserProject(userId, this.id)
    .subscribe(
        data => {
          const position = this.userProject.findIndex(
            (projectEl) => { return projectEl.user_id == data.json().userId; }
          );
          this.userProject.splice(position, 1);
        }
    );
  }

}