import { Injectable } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";
import 'rxjs/Rx';
import { Observable } from "rxjs";

import { Meeting } from "./meeting.interface";


@Injectable()
export class MeetingService {
	token = localStorage.getItem('id_token');

    constructor(private http: Http) { }

  	getMeetings(projectId: number): Observable<any> {
		const body = JSON.stringify({projectId: projectId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/meeting', body, {headers: headers});
	}

	createMeeting(name: string, projectId: number): Observable<any> {
		const body = JSON.stringify({name: name, projectId: projectId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/meeting/create', body, {headers: headers});
	}

	editMeeting(name: string, meetingId: number) {
		const body = JSON.stringify({name: name, meetingId: meetingId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.put('http://project-history.lc/api/meeting/edit/' + meetingId, body, {headers: headers});
	}

	deleteMeeting(meetingId: number) {
		const headers = new Headers({'Authorization': 'Bearer' + this.token});
		return this.http.delete('http://project-history.lc/api/meeting/delete/' + meetingId, {headers: headers});
	}

	getMeeting(meetingId: number): Observable<any> {
		const body = JSON.stringify({meetingId: meetingId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/meeting/get', body, {headers: headers});
	}

	getMeetingPlan(meetingId: number) {
		const body = JSON.stringify({meetingId: meetingId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/meeting/' + meetingId + '/plan', body, {headers: headers});
	}

	updatePlan(plan: string, meetingId: number, stage: boolean) {
		const body = JSON.stringify({content: plan, meetingId: meetingId, stage: stage});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.put('http://project-history.lc/api/meeting/' + meetingId + '/plan/edit', body, {headers: headers});
	
	}

	getMeetingReport(meetingId: number) {
		const body = JSON.stringify({meetingId: meetingId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/meeting/' + meetingId + '/report', body, {headers: headers});
	}

	updateReport(meetingId: number, content: string) {
		const body = JSON.stringify({meetingId: meetingId, content: content});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.put('http://project-history.lc/api/meeting/' + meetingId + '/report/edit', body, {headers: headers});	
	}

	getMeetingTasks(meetingId: number) {
		const body = JSON.stringify({meetingId: meetingId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/meeting/' + meetingId + '/task', body, {headers: headers});
	}

	createMeetingTask(meetingId: number, content: string): Observable<any> {
		const body = JSON.stringify({meetingId: meetingId, content: content});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/meeting/' + meetingId + '/task/create', body, {headers: headers});
	}

	editMeetingTask(meetingId: number, content: string, meetingTaskId: number) {
		const body = JSON.stringify({content: content, meetingTaskId: meetingTaskId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.put('http://project-history.lc/api/meeting/' + meetingId + '/task/' + meetingTaskId + '/edit', body, {headers: headers});
	}

	deleteMeetingTask(meetingId: number, meetingTaskId: number) {
		const headers = new Headers({'Authorization': 'Bearer' + this.token});
		return this.http.delete('http://project-history.lc/api/meeting/' + meetingId + '/task/' + meetingTaskId + '/delete', {headers: headers});
	}

	getConfirmReport(meetingId: number, meetingReportId: number) {
		const body = JSON.stringify({meetingReportId: meetingReportId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/meeting/' + meetingId + '/report/' + meetingReportId + '/confirm', body, {headers: headers});
	}

	confirmReport(meetingId: number, meetingReportId: number) {
		const body = JSON.stringify({meetingReportId: meetingReportId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.put('http://project-history.lc/api/meeting/' + meetingId + '/report/' + meetingReportId + '/confirm/create', body, {headers: headers});
	}

	rejectReport(meetingId: number, meetingReportId: number) {
		const body = JSON.stringify({meetingReportId: meetingReportId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/meeting/' + meetingId + '/report/' + meetingReportId + '/confirm/delete', body, {headers: headers});
	}

	getMeetingUser(meetingId: number, meetingReportConfirm): Observable<any> {
		const body = JSON.stringify({meetingId: meetingId, meetingReportConfirm: meetingReportConfirm});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/meeting/' + meetingId + '/user', body, {headers: headers});
	}

	createMeetingUser(meetingId: number, userId: number): Observable<any> {
		const body = JSON.stringify({meetingId: meetingId, userId: userId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/meeting/' + meetingId + '/user/create', body, {headers: headers});
	}

	deleteMeetingUser(meetingId: number, userId: number): Observable<any> {
		const body = JSON.stringify({meetingId: meetingId, userId: userId});
		const headers = new Headers({'Authorization': 'Bearer' + this.token, 'Content-Type': 'application/json'});
		return this.http.post('http://project-history.lc/api/meeting/' + meetingId + '/user/delete', body, {headers: headers});
	}
}
