import { Component, OnInit, OnDestroy, ChangeDetectorRef, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Response } from "@angular/http";
import { NgForm } from '@angular/forms';
import { default as swal } from 'sweetalert2';

import { AppComponent } from "../../app.component";
import { MeetingService } from "../meeting.service";
import { MeetingTask } from "../meetingTask.interface";
import { ProjectService } from "../../project/project.service";

@Component({
  selector: 'app-show-meeting',
  templateUrl: './show-meeting.component.html',
  styleUrls: ['./show-meeting.component.css']
})
export class ShowMeetingComponent implements OnInit, OnDestroy {
  private id: number;
  private subscription: Subscription;
  userRole = localStorage.getItem('role');

  projectName: string;
  projectId: number;
  meetingName: string;
  planBefore: string;
  planAfter: string;
  stage: boolean;
  meetingTasks: MeetingTask[];
  meetingTask: MeetingTask;
  meetingReport: string;
  meetingReportId: number;
  meetingReportConfirm = [];
  clickSave: boolean;

  @Input() userProject = [];
  meetingUsers = [];
  meetingUser;

  voteAll;
  voteStatus: boolean;
  view;

  constructor(
  	private activateRoute: ActivatedRoute,
  	private meetingService: MeetingService,
    private projectService: ProjectService,
    private cdRef: ChangeDetectorRef,
    private app: AppComponent
  ) { 
    this.subscription = activateRoute.params.subscribe(
      params => {
        this.projectId = params['project_id'];
        this.id = params['id'];
      }
    );
  }

  getMeeting(): void {
    this.meetingService.getMeeting(this.id)
      .subscribe(
        (data: Response) => {
          this.projectName = data.json().projectName;
          this.meetingName = data.json().meetingName;
        }
      );
  }

  getMeetingPlan(): void {
  	this.meetingService.getMeetingPlan(this.id)
  		.subscribe(
  			(data: Response) => {
  				if (data.json().meetingPlanBefore) {
  					this.planBefore = data.json().meetingPlanBefore.content;
  				};
  				if (data.json().meetingPlanAfter) {
  					this.planAfter = data.json().meetingPlanAfter.content;
  				};
  			}
  		);
  }

  getMeetingTasks(): void {
  	this.meetingService.getMeetingTasks(this.id)
      .subscribe(
        (data: Response) => {
          this.meetingTasks = data.json().meetingTasks;
          }
        );
  }

  getMeetingReport(): void {
    this.meetingService.getMeetingReport(this.id)
      .subscribe(
        (data: Response) => {
          if (data.json().meetingReport) {
            this.meetingReport = data.json().meetingReport.content;
            this.meetingReportId = data.json().meetingReport.id;
            if (data.json().meetingReport.confirmation == 1) {
              this.clickSave = true;
            }
            this.getConfirmReport(this.meetingReportId);
          } else {
            let meetingReportConfirm = '';
            this.ifVote(meetingReportConfirm);
          }
        }
      );
  }

  getConfirmReport(meetingReportId): void {
    this.meetingService.getConfirmReport(this.id, meetingReportId)
        .subscribe(
          (data: Response) => {
            this.meetingReportConfirm = data.json().meetingReportConfirm;
            const confirm = this.meetingReportConfirm.find(
              (confirm) => {
                return confirm.user_id == this.app.userId;
              }
            );
            if (confirm) {
              this.voteStatus = true;
            }
            this.ifVote(this.meetingReportConfirm);
          }
        );
  }

  ifVote(meetingReportConfirm): void {
    this.projectService.getUserProject(this.projectId)
      .subscribe(
          (data: Response) => {
            this.userProject = data.json().userProject;

              this.meetingService.getMeetingUser(this.id, meetingReportConfirm)
                .subscribe(
                    (data: Response) => {
                      this.meetingUsers = data.json().meetingUsers;

                      this.view = this.meetingUsers.find(
                        (user) => {
                          return user.id == this.app.userId;
                        });

                      const position = this.meetingUsers.findIndex(
                        (user) => {
                          return user.id == this.app.userId;
                        });

                      this.meetingUsers.splice(position, 1);
                    }
                );
          }
      );
  }

  ngOnInit(): void {
    this.getMeeting();
  	this.getMeetingReport();
    this.getMeetingPlan();
    this.getMeetingTasks();
  }

  ngAfterViewChecked()
  {
    this.cdRef.detectChanges();
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  updatePlanBefore() {
  	this.stage = false;
  	this.updatePlan(this.planBefore, this.stage);
  }

  updatePlanAfter() {
  	this.stage = true;
  	this.updatePlan(this.planAfter, this.stage);
  }

  updatePlan(plan: string, stage: boolean) {
  	this.meetingService.updatePlan(plan, this.id, stage)
	.subscribe(
		() => {
			console.log('Plan save');
		}
	);
  swal({
    title: "План сохранён!",
    type: "success"
  })
  }

  updateReport() {
    this.meetingService.updateReport(this.id, this.meetingReport)
    .subscribe(
      (data) => {
        this.meetingReportId = data.json().meetingReport.id;
      }
    );
    swal({
      title: "Отчёт сохранён! Проголосуйте.",
      type: "success"
    })
    this.clickSave = true;
  }

  deleteTaskElement(meetingTaskId: number) {
    const position = this.meetingTasks.findIndex(
      (meetingEl: MeetingTask) => {
        return meetingEl.id == meetingTaskId;
      }
    );
    this.meetingTasks.splice(position, 1);
  }

  onSubmit(form: NgForm) {
    this.meetingService.createMeetingTask(this.id, form.value.content)
      .subscribe(
        (data: Response) => {
          this.meetingTask = data.json().meetingTask;
          console.log('Meeting task created');
          this.meetingTasks.unshift(this.meetingTask);
        }
      );
    form.reset();
  }

  confirmReport() {
    this.meetingService.confirmReport(this.id, this.meetingReportId)
      .subscribe(
        (data: Response) => {
          this.voteStatus = data.json().meetingReportConfirm.confirm;
        }
      );
  }

  rejectReport() {
    this.meetingService.rejectReport(this.id, this.meetingReportId)
      .subscribe(
        (data: Response) => {
          this.userProject.map(function(user) {
            user.vote = '';
          });
        }
      );
    this.clickSave = false;
    this.voteStatus = false;
  }

  createMeetingUser() {
    if (this.meetingUser) {
      const diff = this.meetingUsers.find(
        (user) => {
          return user.id == this.meetingUser.id;
        });

      if (!diff) {
        this.meetingService.createMeetingUser(this.id, this.meetingUser.id)
          .subscribe(
            (data: Response) => {
              this.meetingUsers.push(data.json().meetingUser);
            }
          );
        this.meetingUser = {};
      } else {
        swal({
          title: "Пользователь уже участвует во встрече!",
          type: "error"
        })
        this.meetingUser = {};
      }
    }
  }

  deleteMeetingUser(userId) {
    this.meetingService.deleteMeetingUser(this.id, userId)
      .subscribe(
        (data: Response) => {
          const position = this.meetingUsers.findIndex(
            (user) => {
              return user.id == userId;
          });
          this.meetingUsers.splice(position, 1);
        }
      );
  }

  
}