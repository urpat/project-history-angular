import { Component, OnInit, Input } from '@angular/core';
import { Response } from "@angular/http";

import { MeetingService } from "../meeting.service";
import { MeetingTask } from "../meetingTask.interface";
import { ShowMeetingComponent } from "../show-meeting/show-meeting.component";

@Component({
  selector: 'app-meeting-task',
  templateUrl: './meeting-task.component.html',
  styleUrls: ['./meeting-task.component.css']
})
export class MeetingTaskComponent implements OnInit {
  @Input() meetingTask: MeetingTask;
  // meetings: Meeting[];
  edit = false;
  editValue = '';

  constructor(
  	private meetingService: MeetingService,
  	private showMeetingComponent: ShowMeetingComponent,
  ) {}

  ngOnInit() {

  }

  onEdit() {
  	this.edit = true;
  	this.editValue = this.meetingTask.content;
  }

  onCancel() {
  	this.edit = false;
  }

  onUpdate() {
  	this.meetingService.editMeetingTask(this.meetingTask.meeting_id, this.editValue, this.meetingTask.id)
  		.subscribe(
  			(meetingTask: Response) => {
  				console.log('Meeting task edit');
  				this.meetingTask.content = this.editValue;
  				this.editValue = '';
  			}
  		);
  	this.edit = false;
  }

  onDelete() {
  	this.meetingService.deleteMeetingTask(this.meetingTask.meeting_id, this.meetingTask.id)
  		.subscribe(
  			(meetingTaskId: Response) => {
  				console.log('Meeting Task delete');
  				this.showMeetingComponent.deleteTaskElement(meetingTaskId.json().meetingTaskId);
  			}
  		);
  }

}
