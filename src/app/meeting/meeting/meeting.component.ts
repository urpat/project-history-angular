import { Component, Input, OnInit } from '@angular/core';

import { Response } from "@angular/http";

import { Meeting } from "../meeting.interface";
import { MeetingService } from "../meeting.service";
import { MeetingsComponent } from "../meetings/meetings.component";
import { AppComponent } from "../../app.component";

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.css']
})
export class MeetingComponent implements OnInit {
  @Input() meeting: Meeting;
  meetings: Meeting[];
  projectId;
  edit = false;
  editValue = '';

  constructor(
  	private meetingService: MeetingService,
  	private meetingsComponent: MeetingsComponent,
    private app: AppComponent
  ) {}

  ngOnInit() {
    this.projectId = this.meetingsComponent.id;
  }

  onEdit() {
  	this.edit = true;
  	this.editValue = this.meeting.name;
  }

  onCancel() {
  	this.edit = false;
  }

  onUpdate() {
  	this.meetingService.editMeeting(this.editValue, this.meeting.id)
  		.subscribe(
  			(meeting: Response) => {
  				console.log('Meeting edit');
  				this.meeting.name = this.editValue;
  				this.editValue = '';
  			}
  		);
  	this.edit = false;
  }

  onDelete() {
  	this.meetingService.deleteMeeting(this.meeting.id)
  		.subscribe(
  			(meetingId: Response) => {
  				console.log('Meeting delete');
  				this.meetingsComponent.deleteElement(meetingId.json().meetingId);
  			}
  		);
  }

}