export interface Meeting {
	id: number;
	project_id: number;
	name: string;
}