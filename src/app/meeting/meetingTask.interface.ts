export interface MeetingTask {
	id: number;
	meeting_id: number;
	content: string;
}